
package views;

import java.awt.Frame;

import misc.events.FormInterface;

public class WFrame extends Frame implements FormInterface {
	Frame parentFrame;
	public WFrame(MainFrame parentFrame) {
		this.parentFrame = parentFrame;
	}
	@Override
	public void initComponents() {
			
	}

	@Override
	public void showForm() {
			
	}

	@Override
	public void clean() {
		
	}

	@Override
	public void showForm(boolean maximize) {
			
	}

	
}

