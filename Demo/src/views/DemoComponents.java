
package views;

import java.awt.*;
import views.components.panels.BorderPanel;

public class DemoComponents extends WFrame {
    private Label label1;
    private TextField textField1;
    private Label label2;
    private TextField textField2;
    private Label label3;
    private TextArea textArea1;
    private Label label4;
    private Choice choice1;
    private Panel panel1;
    private Checkbox checkbox1;
    private Checkbox checkbox2;
    private Checkbox checkbox3;
    private BorderPanel panel2;
    private CheckboxGroup group1;
    private Checkbox checkboxA;
    private Checkbox checkboxB;
    private Checkbox checkboxC;
    private List list1;
    private Scrollbar scrollbar1;
    private Scrollbar scrollbar2;
    
    private Canvas canvas1;
    
    private Button button1;
    private Button button2;
    
    
    public DemoComponents(MainFrame parentFrame) {
        super(parentFrame);
        initComponents();
    }

    public void initComponents(){
        label1 = new Label("Etiqueta 1");
        label1.setBounds(20, 50, 100, 32);
        add(label1);
        
        textField1 =new TextField("");
        textField1.setBounds(122,50,250,32);
        add(textField1);
        
        label2 = new Label("Etiqueta 2");
        label2.setBounds(20, 85, 100, 32);
        add(label2);
        
        textField2 =new TextField("");
        textField2.setBounds(122,85,250,32);
        textField2.setEnabled(false);
        add(textField2);
        
        label3 = new Label("Descripcion");
        label3.setBounds(20, 120, 100, 32);
        add(label3);
        
        textArea1 = new TextArea();
        textArea1.setBounds(20,155,350,90);
        add(textArea1);
        
        panel1 = new Panel();
        panel1.setBounds(375,50,100,90);
        panel1.setBackground(Color.lightGray);
        
        checkbox1 = new Checkbox("opcion 1");
        checkbox2 = new Checkbox("opcion 2");
        checkbox3 = new Checkbox("opcion 3");
        
        panel1.add(checkbox1);
        panel1.add(checkbox2);
        panel1.add(checkbox3);
        add(panel1);
        
        panel2 = new BorderPanel();
        panel2.setBorderVisible(true);
        panel2.setBounds(375, 155, 100, 90);
        
        group1 = new CheckboxGroup();
        checkboxA = new Checkbox("Opcion A",group1,true);
        checkboxB = new Checkbox("Opcion B",group1,false);
        checkboxC = new Checkbox("Opcion C",group1,false);
        
        panel2.add(checkboxA);
        panel2.add(checkboxB);
        panel2.add(checkboxC);
        add(panel2);
        
        label4 = new Label("Selecciona");
        label3.setBounds(20, 260, 100, 32);
        add(label4);
        
        choice1 = new Choice();
        choice1.setBounds(122,260,250,32);
        choice1.addItem("Rojo");
        choice1.addItem("Amarillo");
        choice1.addItem("Azul");
        add(choice1);
        
        list1 = new List();
        list1.setBounds(20, 295, 350, 90);
        list1.add("Manzana");
        list1.add("Pera");
        list1.add("Uva");
        list1.add("Fresa");
        list1.add("Jocote");
        list1.add("Mango");
        add(list1);
        
        scrollbar1 = new Scrollbar();
        scrollbar1.setOrientation(Scrollbar.VERTICAL);
        scrollbar1.setBounds(460,295,16,80);
        add(scrollbar1);
        
        scrollbar2 = new Scrollbar();
        scrollbar2.setOrientation(Scrollbar.HORIZONTAL);
        scrollbar2.setBounds(375,375,90,16);
        add(scrollbar2);
        
        canvas1 = new Canvas();
        canvas1.setBounds(20,400,450,100);
        canvas1.setBackground(Color.CYAN);
        add(canvas1);
        
        button1.setBounds(20, 510, 100, 32);
        button2 = new Button("Boton 2");
        button2.setBounds(370, 510, 100, 32);
        
        add(button1);
        add(button2);
        
        setTitle("Componentes");
        setSize(500,600);
   }
  
}
