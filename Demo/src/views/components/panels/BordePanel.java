
package views.components.panels;

import java.awt.Graphics;
import java.awt.Panel;
import java.awt.Rectangle;


public class BordePanel extends Panel {
    
    private boolean drawBorder = true;
    private int bordeOffset = 2;
    
    public void setBorderVisible (boolean b){
        if (b != drawBorder){
            drawBorder = b;
            repaint();
        }
    }
    public boolean isBorderVisible(){
        return drawBorder;
    }
    public void setBorderOffset(int i){
        bordeOffset = i;
        repaint();
    }
    
    public int getBordeOffSet(){
        return bordeOffset;
    }
    
    protected Rectangle getBorderBound(){
       int x=bordeOffset;
       int y=bordeOffset;
       int width = getSize().width - bordeOffset *2;
       int height = getSize().height - bordeOffset *2;
       Rectangle bounds = new Rectangle(x,y,width,height);
       return bounds;
    }
    public void update (Graphics G){
        paint(G);
    }
    
    public void paint (Graphics G){
        G.setColor(getBackground());
        G.fillRect(0, 0, getSize().width, getSize().height);
        G.setColor(getForeground());
        Rectangle border = getBorderBound();
        G.drawRect(border.x, border.y, border.width, border.height);
    }
}
