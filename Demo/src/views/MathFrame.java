
package views;

import java.awt.*;



public class MathFrame extends WFrame {
    private Label label1,label2,label3;
    private TextField textfield1,textfield2,textfield3;
    private CheckboxGroup group1;
    private Checkbox checkbox1,checkbox2,checkbox3,checkbox4;
    private Button button1,button2;
    
    public MathFrame(Frame parentFrame) {
        super(parentFrame);
        initComponents();
    }

    
    @Override
    public void initComponents(){
         setLayout(null);
         label1= new Label("Numero1");label1.setBounds(20, 50, 100, 32);add(label1);
         textfield1= new TextField("");textfield1.setBounds(122, 50, 250, 32);add(textfield1);
         label2= new Label("Numero2");label2.setBounds(20, 84, 100, 32);add(label2);
         textfield2= new TextField("");textfield2.setBounds(122, 84, 250, 32);add(textfield2);
         label3= new Label("Resultado");label3.setBounds(20, 118, 100, 32);add(label3);
         textfield3= new TextField("");textfield3.setBounds(122, 118, 250, 32);
         textfield3.setEnabled(false);add(textfield3);
         
         group1 = new CheckboxGroup();
         checkbox1 = new Checkbox("Suma",group1,true);checkbox1.setBounds(380, 50, 100,24);add(checkbox1);
         checkbox2 = new Checkbox("Resta",group1,false);checkbox2.setBounds(380, 75, 100,24);add(checkbox2);
         checkbox3 = new Checkbox("Multiplicacion",group1,false);checkbox3.setBounds(380, 100, 100,24);add(checkbox3);
         checkbox4 = new Checkbox("Suma",group1,false);checkbox4.setBounds(380, 125, 100,24);add(checkbox4);
         
         button1 = new Button("Borrar");button1.setBounds(20, 175, 100, 32);add(button1);
         button2 = new Button("Calcular");button2.setBounds(370, 175, 100, 32);add(button2);
         
         setTitle("Operaciones Matematicas");
         setSize(500,250);
    }
     
   

    
}
